import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {FormControl} from '@angular/forms';
@Component({
  selector: 'app-newtask',
  templateUrl: './newtask.component.html',
  styleUrls: ['./newtask.component.scss']
})
export class NewtaskComponent implements OnInit {
  items;
  checkoutForm;
  tasks:any;
  isShowDate: boolean = false;
  toppings = new FormControl();
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  toppingList: string[] = ['Education', 'Social Activities', 'Health', 'Office Work', 'Personal Work'];
  constructor( private formBuilder: FormBuilder,private _formBuilder: FormBuilder) { 
    
     this.checkoutForm = this.formBuilder.group({
      name: '',
      address: ''
    }); }

  ngOnInit() {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      thirdCtrl: ['', Validators.required]
    });
  }

  checkedDate(val)
  {
    if(val==1)
    {
      
      this.isShowDate=false;
    }
    else
    {
      this.isShowDate=true;
    }
  }

  }

