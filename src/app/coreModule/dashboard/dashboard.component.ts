import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor() { }
  currentDate  = moment(new Date()).format("MMM DD YYYY");
  day = moment(new Date()).format('dddd');
  username: string = 'Rahul';
  ngOnInit() {
  }

}
