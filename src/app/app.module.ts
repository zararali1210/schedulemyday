import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Routes, RouterModule, Router, Route } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatButtonModule} from '@angular/material/button';
import {MatStepperModule} from '@angular/material/stepper';
import {MatRadioModule} from '@angular/material/radio';
import { AppComponent } from './app.component';
import { LoginComponent } from './coreModule/login/login.component';
import { DashboardComponent } from './coreModule/dashboard/dashboard.component';
import { NewtaskComponent } from './coreModule/newtask/newtask.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FooterComponent } from './coreModule/footer/footer.component';
import { HeaderComponent } from './coreModule/header/header.component';
import { MatFormFieldModule, MatNativeDateModule  } from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material';
import {MatSelectModule} from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';

import { TaskviewComponent } from './coreModule/taskview/taskview.component';
//import { MatMomentDateModule, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';


const routes: Routes = [ 
  {path: '',  component : TaskviewComponent },
  { path: 'newtask', component :NewtaskComponent},
  { path: 'home', component :DashboardComponent},
  { path: 'login', component :LoginComponent},
  {path: 'task-view', component: TaskviewComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    NewtaskComponent,
    FooterComponent,
    HeaderComponent,
    TaskviewComponent,
    HeaderComponent
    
  ],
  imports: [
    
    BrowserModule,
    MatCardModule,
    AppRoutingModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatStepperModule,
    MatRadioModule,
    FormsModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatInputModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    NgxMaterialTimepickerModule,
    AppRoutingModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  providers: [ 
  //  {provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: {useUtc: true}}
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
