import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  isShowLogin: boolean = false;
  isShowSignUp: boolean= false;
  constructor() { }

  ngOnInit() {
  }

  login(val) {
    if (val == 1) {
      this.isShowLogin = false;

    }

  }

  signUp(val) {
    if (val == 2) {
      this.isShowLogin = true;
    }
  }



}
